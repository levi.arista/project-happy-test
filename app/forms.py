from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField
from wtforms.validators import DataRequired

class LoginForm(FlaskForm):
    username = StringField(
        'Project Happy User Name or Email',
        validators=[DataRequired()],
        render_kw={"placeholder": "Project Happy User Name or Email"}
    )
    password = PasswordField(
        'Secret Password',
        validators=[DataRequired()],
        render_kw={"placeholder": "Secret Password"}
    )
    submit = SubmitField('Sign in')
