from flask import render_template, session, redirect, url_for, request, flash
from app.forms import LoginForm
from app import login_manager
from flask_sqlalchemy import SQLAlchemy
from flask_login import login_user, login_required, logout_user, current_user
from app.models import User, get_user

from . import auth

@login_manager.user_loader
def load_user(user_id):
    """Check if user is logged-in on every page load."""
    if user_id is not None:
        return User.query.get(user_id)
    return None

@auth.get('/login')
def login():
    if current_user.is_authenticated:
        return redirect(url_for('dashboard'))
    context = {
        'login_form': LoginForm(),
    }
    return render_template('login.html', **context)


@auth.post('/login')
def do_login():
    login_form = LoginForm()
    if login_form.validate_on_submit():
        username = login_form.username.data
        password = login_form.password.data

        user = get_user(username)

        if user and user.check_password(password=password):
            login_user(user)
            next_page = request.args.get('next')
            session['username'] = username
            return redirect(next_page or url_for('dashboard'))

        flash('Invalid username/password combination')
        return redirect(url_for('auth.login'))

    return redirect(url_for('auth.login'))

@auth.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('index'))
