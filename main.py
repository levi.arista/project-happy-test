from flask import render_template, session
from app import create_app
from app.models import get_opportunities, get_opportunity
from flask_login import login_required

app = create_app()


@app.route('/')
def index():
    return render_template('index.html')


@app.get('/dashboard')
@login_required
def dashboard():
    opportunities = get_opportunities()
    context = {
        'username': session.get('username'),
        'opportunities': opportunities
    }
    return render_template('dashboard.html', **context)


@app.get('/opportunities/<opportunity_id>')
@login_required
def opportunity(opportunity_id):
    opportunity = get_opportunity(id=opportunity_id)
    context = {
        'opportunity': opportunity
    }
    return render_template('opportunity.html', **context)


@app.errorhandler(404)
def page_not_found(error):
    return render_template('404.html', error=error)
